[
  {
    "name": "id",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "Id of user"
  },
  {
    "name": "first_name",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "First name of the user"
  },
  {
    "name": "last_name",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "Last name of the user"
  },
  {
    "name": "age",
    "type": "INT64",
    "mode": "NULLABLE",
    "description": "Age of the user"
  }
]