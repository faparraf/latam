declare -a funcs=("read_bq" "write_bq_pub")

for i in "${funcs[@]}"
do
    echo "Zipping $i"
    cd $i
    zip -r ../$i.zip *
    cd ..
done

echo "Zipping finished"