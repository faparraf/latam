
import functions_framework
from google.cloud import bigquery
from markupsafe import escape

def query(id=None):
    client = bigquery.Client()
    query = "SELECT id, first_name, last_name, age FROM `latam_dataset.users` LIMIT 10"
    if id:
        query = f"SELECT id, first_name, last_name, age FROM `latam_dataset.users` WHERE id='{id}'"
    query_job = client.query(query)
    rows = [dict(row) for row in query_job.result()]
    return rows

@functions_framework.http
def listener(request):
    request_json = request.get_json(silent=True)
    request_args = request.args
    id = None
    if request.method != "GET":
        return "Invalid method"
    if request_args and "id" in request_args:
        id=request_args["id"]
    results = query(id)
    return results

