
import functions_framework
from google.cloud import pubsub_v1
import os
import json

@functions_framework.http
def listener(request):

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(os.environ['GCLOUD_PROJECT'], os.environ['TOPIC_ID'])

    request_json = request.get_json(silent=True)
    request_args = request.args
    
    data_str = json.dumps(request_json)
    data = data_str.encode("utf-8")
    print(data)
    future = publisher.publish(topic_path, data)
    return future.result()
