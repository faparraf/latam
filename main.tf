terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "5.35.0"
    }
  }
}

locals {
  project = "latam-test-427819"
  region = "us-central1"
  location = "US"
  dataset_id = "latam_dataset"
  functions_folders = toset(["read_bq", "write_bq_pub"])
}

provider "google" {
  project     = local.project
  region      = local.region
}

data "google_project" "project" {
}

resource "google_service_account" "service_account" {
  account_id = "latam-sa"
  display_name = "LATAM service account"
}

resource "google_pubsub_topic" "topic" {
  name = "data-topic"
}

resource "google_pubsub_subscription" "write_subs" {
  name  = "latam-write-subscription"
  topic = google_pubsub_topic.topic.id

  bigquery_config {
    table = "${google_bigquery_table.users.project}.${google_bigquery_table.users.dataset_id}.${google_bigquery_table.users.table_id}"
    service_account_email = google_service_account.service_account.email
    write_metadata = false
    use_table_schema = true
  }
}

resource "google_bigquery_dataset" "dataset" {
  dataset_id                  = local.dataset_id
  friendly_name               = "dataset"
  description                 = "This is a test for latam dataset"
  location                    = local.location
}

resource "google_bigquery_dataset_iam_member" "bq_admin_access" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  role = "roles/bigquery.admin"
  member = "serviceAccount:${google_service_account.service_account.email}"
}

resource "google_project_iam_member" "project_admin_access" {
    project = local.project
    role = "roles/owner"
    member = "serviceAccount:${google_service_account.service_account.email}"
}

resource "google_bigquery_dataset_iam_member" "pubsub_bq_admin_access" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  role = "roles/bigquery.dataEditor"
  member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}

resource "google_project_iam_member" "project_pubsub_bq_admin_access" {
    project = local.project
    role = "roles/bigquery.dataEditor"
    member = "serviceAccount:service-${data.google_project.project.number}@gcp-sa-pubsub.iam.gserviceaccount.com"
}

resource "google_pubsub_subscription_iam_member" "pubsub_admin_access" {
  subscription = google_pubsub_subscription.write_subs.name
  role = "roles/pubsub.admin"
  member = "serviceAccount:${google_service_account.service_account.email}"
}

resource "google_project_iam_member" "project_pubsub_admin_access" {
    project = local.project
    role = "roles/pubsub.admin"
    member = "serviceAccount:${google_service_account.service_account.email}"
}

data "local_file" "users_schema" {
  filename = "users_schema.txt"
}

resource "google_bigquery_table" "users" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = "users"
  deletion_protection = true
  schema = data.local_file.users_schema.content

}

resource "google_storage_bucket" "bucket" {
  name     = "${local.project}-gcf-source"
  location = local.location
  uniform_bucket_level_access = true
}

data "archive_file" "function_zip" {
  for_each = local.functions_folders
  type        = "zip"
  output_path = "functions/${each.value}.zip"
  source_dir  = "functions/${each.value}/"
}

resource "google_storage_bucket_object" "storage_object" {
  for_each = local.functions_folders
  name   = each.value
  bucket = google_storage_bucket.bucket.name
  source = data.archive_file.function_zip[each.value].output_path
  depends_on = [data.archive_file.function_zip]
}

resource "google_cloudfunctions2_function" "function_read_bq" {
  name = "read_bq"
  location = local.region
  description = "Read BigQuery function"
  depends_on = [google_storage_bucket_object.storage_object]

  build_config {
    runtime = "python312"
    entry_point = "listener"
    source {
      storage_source {
        bucket = google_storage_bucket.bucket.name
        object = google_storage_bucket_object.storage_object["read_bq"].name
      }
    }
  }

  service_config {
    max_instance_count  = 1
    available_memory    = "256M"
    timeout_seconds     = 61
    min_instance_count = 1
    service_account_email = google_service_account.service_account.email
    environment_variables = {
        LOG_EXECUTION_ID = "true"
        GCLOUD_PROJECT =local.project
        VERSION ="1"
    }
  }
}

resource "google_cloudfunctions2_function" "function_write_bq_pub" {
  name = "write_bq_pub"
  location = local.region
  description = "Write on Pub/Sub service"
  depends_on = [google_storage_bucket_object.storage_object]

  build_config {
    runtime = "python312"
    entry_point = "listener"
    source {
      storage_source {
        bucket = google_storage_bucket.bucket.name
        object = google_storage_bucket_object.storage_object["write_bq_pub"].name
      }
    }
  }

  service_config {
    max_instance_count  = 1
    available_memory    = "256M"
    timeout_seconds     = 60
    min_instance_count = 1
    service_account_email = google_service_account.service_account.email
    environment_variables = {
        LOG_EXECUTION_ID = "true"
        GCLOUD_PROJECT =local.project
        TOPIC_ID =google_pubsub_topic.topic.name
    }
  }
}

output "read_bq_uri" {
  value = google_cloudfunctions2_function.function_read_bq.url
}
output "write_bq_uri" {
  value = google_cloudfunctions2_function.function_write_bq_pub.url
}
output "topic_id" {
  value = google_pubsub_topic.topic.id
}
