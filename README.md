# Ejercicio LATAM

## Arquitectura
![alt text](assets/LATAM.jpg "Arquitectura")

La arquitectura está basada enteramente en un sistema serverless con 3 servicios principales:
- Bigquery
- Pub/Sub
- Cloud functions

### Ingesta de datos

La ingesta de datos se hace por medio de una Cloud Function con expsición https que recibe un payload (el mismo esquema de BigQuery) y lo publica en el tópico creado para PubSub.
Asociado a dicho tópico hay una subscripción ed Pub/Sub con direccionamiento a BigQuery que insertará el registro en la tabla especificada.

La petición se puede hacer con una petición POST de la siguiente forma

    curl -m 70 -X POST "<URL>/write_bq_pub" \
    -H "Authorization: bearer $(gcloud auth print-identity-token)" \
    -H "Content-Type: application/json" \
    -d '{
    "id": "test_id", "first_name": "Fabio", "last_name": "Parra", "age": 33
    }'

o si se desea, publicando directamente en la cola

    gcloud pubsub topics publish <TOPIC> --message='{"id": "test_id", "first_name": "Fabio", "last_name": "Parra", "age": 33}'

### recepción de datos
La peticion de datos se hace por medio del endopint https GET expuesto por cloud functions, de la siguiente manera

    curl -m 70 -X GET "<URL>/read_bq?id=<ID>" \
    -H "Authorization: bearer $(gcloud auth print-identity-token)" \
    -H "Content-Type: application/json"

en donde, si se cuenta con un id específico para consultar puede ser agregado a la url como parámetro, de lo contrario la función traerá los 10 primeros registros que encuentre (en toda la base de datos)

## Mejoras
Debido a limitaciones en el tiempo hubo deuda técnica que debería atacarse en un ambiente real
- Solo se implementó CI en una de las ramas (develop) así como la publicación de los artefactos sin taggear o identificar, una mejora podría ser implementar marcas en los artefactos a la hora de publicar y desplegar, con el fin de identificar la aplicación a la que pertenece así como su versión, mediante estrategia de commit sha o ramas podría hacerse.
- Las variables de proyecto deberían estar correctamente aisladas y reutilizadas en terraform
- La definición de las funciones y sus artefactos podrían estar en un módulo aparte, así como los permisos necesarios

## Pruebas

Se propone una prueba en donde un registro con un id específico es buscado en la base de datos, mediante el endpoint https generado por cloud functions, mediante una petición curl

### Pruebas extra propuestas
**Proponer otras pruebas de integración que validen que el sistema está funcionando correctamente y cómo se implementarían**
#### Uso de una suite de pruebas
La prueba implementada se hizo directamente sobre bash. Dichan implementación básica no ofrece información de la calidad de las pruebas ni del coverage de las mismas. Para robustecer la plataforma se sugiere el uso de herramientas especializadas en testing, **pytest** por ejemplo, lo que nos daría mayor visibilidad y traza de las pruebas, así como una forma más amigable para el desarrollador y el equipo en general de extender las pruebas y mejorarlas

#### Creación de registros mock
Una prueba para la infra y su capacidad de entregar datos es llenar la tabla (ambiente develop) con registros de prueba y buscar al azar registros.

#### Pruebas de manejo de errores
Una forma de probar la resiliencia del sistema y sus enpoints (lectura y publicación) es mediante el envío de peticiones corruptas y posterior validación de la respuesta de las funciones y el estado posterior de la base de datos.

#### Pruebas de carga
Podrían implementarse pruebas más elaboradas de carga en las que se pueba probar la capacidad de la función (Cloud function) de escribir en la cola y escalar correctamente, y por su parte, el endpoint de lectura su capacidad de devolver los registros consultados con tiempos de respuesta adecuados

### Puntos críticos y Robustecimiento
**Identificar posibles puntos críticos del sistema (a nivel de fallo o performance) diferentes al punto anterior y proponer formas de testearlos o medirlos (no implementar)**

#### Datos incompletos en la petición
El modelo actual fallará si la petición tiene tipos de datos incorrectos o incompletos. Esto podría imprimirse en un log especializado y medirse adecuadamente: Stack EKL o base de datos dedicada a logs

#### Schema de la base de datos más limitado
Un schema inadecuado en la base de datos permitirá que la calidad de los datos pueda ser inferior o la no deseada. Esto podría corregirse mapeando los tipos de datos de forma adecuada en la API python y definiendo qué se quiere publicar con los datos recibidos.

#### Baja escalabilidad de la solución propuesta
Como solución a esto (mencionado en las pruebas) podría implementarse una interfaz de API Gateway que ofrezca una escalabilidad aparte de la cloud function, y de ser necesario una arquitectura basada en contenedores despúes del gateway que permita omitir un cold start de las funciones.

#### Fallos de lectura de la cola
Si ocurre algún error a la hora de leer los mensajes de la cola, es necesaria una correcta política de reintento, así como asegurar una idempotencia a la hora de escribir el registro. Esto se podría medir con una consulta de registros duplicados, y una rutina que limpie la base de datos (se puede asumir que es una bodega de datos que permite tener duiplicados)

#### Implementación de caché
Dependiendo de la estructura y tipo de datos que se están consultado, se puede plantear una capa adicional en la lectura de la base de datos en la que se cachee las peticiones habituales. Dpendiento del tipo de datos se puede pensar en un caché de memoria si existen peticiones suelen ser comunes y constantes, ej Redis o functools

#### Partición en la base de datos
Si las consultas tardan demadiado en ejecutarse, puede implementarse un esquema con llaves primarias o particiones para hacer la velocidad de consulta más baja.

## Métricas
### Métricas propuestas
**Proponer 3 métricas (además de las básicas CPU/RAM/DISK USAGE) críticas para entender la salud y rendimiento del sistema end-to-end**
- _**Peticiones http por unidad de tiempo/Códigos 2xx retornados**_
- _**Peticiones http por unidad de tiempo/Códigos 5xx retornados**_
    
    De esta forma se puede evaluar el rendimiento de las cloud functions a la hora de trabajar en un entorno más real

- _**Tiempo de ejecución en cada Cloud Function**_
    
    Acá podríamos evaluar el performance de cada función y encontrar puntos de mejora/optmización

- _**Mensajes de la cola PubSub sin ser leidos**_
    
    Podemos evaluar si la cantidad de replicas que se encargan de leer los mensajes es suficiente o se puede escalar para mejoprar el rendimiento


### Herramienta de visualización
**Proponer una herramienta de visualización y describe textualmente qué métricas mostraría, y cómo esta información nos permitiría entender la salud del sistema para tomar decisiones estratégicas**
La primera opción sería usar Grafana e importar métricas definidas previamente en la nube, sin embargo una buena opción nativa de GCP podría ser Monitoring, en la que podríamos crear tableros Custom, permite importar tableros de grafana y se soluciona la necesidad de publicación y acceso para todo el equipo sin necesidad de disponer de infra adicional o configuraciones locales.

Se podría incluso optar por tener el esquema json del dashboard en un repositorio git y por medio de terraform crear los dashboards para los ambientes necesarios

#### Métricas
Además de las métricas mencionadas anteriormente (Estados de peticiones http, Tiempo de ejecución de cloud functions, Mensajes en la cola), sería importante evaluar los tiempos de las consultas en bigquery, de esta forma podríamos evaluar si establecer particiones para la base de datos es una opción necesaria

Si detrás de la lectura hay una lógica que transforma los datos, las métricas de duración puieden dar indicios de si las funciones están tardando demasiado, en cuyo caso planear ejecuciones en batch para la transformación de un grupo de registros (por fechas por ejemplo) antes de ser consultados, y tener tablas o vistas intermedias que muestren los datos transformados.

Tener métricas sobre los logs, buscando tal vez errores/warnings que no dañan la respuesta per sí generan ruido, podría ser una forma de mejorar el sistema en un futuro, de esta forma se evalúa la "calidad de la ejecución" y se tiene en el tablero para tener claro.

**Describe a grandes rasgos cómo sería la implementación de esta herramienta en la nube y cómo esta recolectaría las métricas del sistema**
Si se usara Monitoring de GCP la implementación de la herramienta ya estaría solucionada de antemano, y el scope de la recolección de las métricas estaría dada por el proyecto en el que se está ejecutando. **Una estrategia de tagging** puede ayudar a separar ambientes y aplicaciones específicas para usar tableros o vistas diferentes, de esta forma los tags asignados a la infra desplegada sirven de filtro a la hora de mostrar las métricas

**Describe cómo cambiará la visualización si escalamos la solución a 50 sistemas similares y qué otras métricas o formas de visualización nos permite desbloquear este escalamiento**

- Una estrategia de buscar tener tableros con medidas estadísticas puede ser una forma inicial de tener visibilidad de todo el ecosistema de la aplicación, de esta forma, solo cuando algo altere las estadísticas (ej. medias de tiempo, o promedio de códigos 2xx) se puede entrar al detalle de los recursos específicos, en lo posible agrupado por tags.
- Dashboards para el ecosistema, y otros específicos para el detalle de una aplicación/scope
- Semáforos del estado/rendimiento para cada aplicación, de esta forma se hace fácil de visualizar cuando una aplicación específica está teniendo problemas, o si es un problema generalizado

**Comenta qué dificultades o limitaciones podrían surgir a nivel de observabilidad de los sistemas de no abordarse correctamente el problema de escalabilidad**

- Si no se pudiera filtrar los recursos correctamente (estrategia de taggeo, o por sufijos) la solución de visualización podría escalar debido a que todas las métricas estarían siendo consultadas, o habría que seleccionar manualmente qué se desea ver, haciéndose casi imposible de hacer seguimiento a una aplicación específica.
- Si no se establece una forma de agrupamiento, hacer seguimiento a todas las métricas/gráficas (ej gráficas de lineas o barras) se va a hacer imposible cuando hayan un alto número de datos

## Alertas y SRE
**Define específicamente qué reglas o umbrales utilizarías para las métricas propuestas, de manera que se disparan alertas al equipo al decaer la performance del sistema**

Los valores en los que las métricas están alertadas o con alarmas va a depender enteramente de la lógica y la evolución del sistema así como los SLAs que se tengan, y sin duda alguna hay que hacer tareas de validación de los thresholds con cierta frecuencia para evaluar si tienen congruencia con el comportamiento medio de los recursos, por ejemplo si una cloudfunction suele tardar 100ms con un margen de variación de +-10ms tiene sentido tener un threshold en 120ms y evaluarlo cada cierto tiempo o después de una optimización del código

- _**Proporción de códigos http devueltos**_
    
    Si el porcentaje de errores 5xx  o 4xx supera 5% (habitual) una alerta debería ser lanzada

- _**Tiempo de ejecución en cada Cloud Function**_
    
    Esto dependería de la lógica que se esté ejecutando en la función

- _**Mensajes de la cola PubSub sin ser leidos**_
    
    Dependera netamente de la necesidad de que los datos enviados al enpoint estén en la base de datos lo antes posible, si se quiere que sea "tiempo real" se esperaria que un mensaje no tarde más de 1000ms, habrá que añadir concurrencia para leer los mensajes, así que una alerta sobre los 1200ms en promedio podría ser adecuada

- _**Tiempo de consulta bigquery**_

    Este tiempo dependerá enteramente de la cantidad de datos y la necesidad a la hora de consumir los datos
