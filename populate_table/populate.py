import pandas as pd
import os
import json

f = open(os.environ['DATA'])
data = json.load(f)

df = pd.read_csv(os.environ['FILENAME'])
df.to_gbq(data["dataset_id"]+"."+data["tablename"], project_id=data["project"])
